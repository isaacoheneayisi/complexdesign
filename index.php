<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SMS VOTING</title>

        <!-- Bootstrap Core CSS -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Font Awesome CSS -->
        <link href="css/font-awesome.min.css" rel="stylesheet">
		
		<!-- Custom CSS -->
        <link href="css/animate.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/style.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>

      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Template js -->
        <script src="js/jquery-2.1.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.appear.js"></script>
        <script src="js/contact_me.js"></script>
        <script src="js/jqBootstrapValidation.js"></script>
        <script src="js/modernizr.custom.js"></script>
        <script src="js/script.js"></script>

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .error{
             color: red
       }

        </style>
    </head>
    
    <body>
        
        <!-- Start Logo Section -->
        <section id="logo-section" class="text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo text-center">
                            <h1>ONLINE SMS VOTING</h1>
                            <span>Improving and enhancing eletronic voting in Africa</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Logo Section -->
        
        
        <!-- Start Main Body Section -->
        <div class="mainbody-section text-center">
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-3">
                        
                        <div class="menu-item blue">
                            <a href="#feature-modal" data-toggle="modal">
                                <i class="fa fa-magic"></i>
                                <p>HOW TO VOTE</p>
                            </a>
                        </div>
                        
                        <div class="menu-item green">
                            <a href="#about-modal" data-toggle="modal">
                                <i class="fa fa-file-photo-o"></i>
                                <p>ABOUT SMS VOTING</p>
                            </a>
                        </div>
                        
                        <div class="menu-item light-red">
                            <a href="#portfolio-modal" data-toggle="modal">
                                <i class="fa fa-user"></i>
                                <p>WHY THIS PROJECT</p>
                            </a>
                        </div>
                        
                    </div>

             
                    
                    <div class="col-md-6">
                        
                        <!-- Start Carousel Section -->

                        <div class="home-slider">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="padding-bottom: 30px;">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                </ol>

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <img src="images/about-03.jpg" class="img-responsive" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="images/about-02.jpg" class="img-responsive" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="images/about-01.jpg" class="img-responsive" alt="">
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- Start Carousel Section -->
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="menu-item color responsive">
                                    <a href="#service-modal" data-toggle="modal">
                                        <i class="fa fa-area-chart"></i>
                                        <p>BENEFITS</p>
                                    </a>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="menu-item light-orange responsive-2">
                                    <a href="#team-modal" data-toggle="modal">
                                        <i class="fa fa-users"></i>
                                        <p>PROJECT TEAM</p>
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                    
                    <div class="col-md-3">
                        
                        <div class="menu-item light-red">
                            <a href="online_project_vote/index.php" data-toggle="modal">
                                <i class="fa fa-comment-o"></i>
                                <p>VOTE PROJECT ONLINE</p>
                            </a>
                        </div>
                        
                        <div class="menu-item color">
                            <a href="online_voting/index.php" data-toggle="modal">
                                <i class="fa fa-comment-o"></i>
                                <p>VOTE ONLINE</p>
                            </a>
                        </div>
                        
                        <div class="menu-item blue">
                            <a href="admin/index.php" data-toggle="modal">
                                <i class="fa fa-address-book"></i>
                                <p>ADMIN</p>
                            </a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- End Main Body Section -->
        
        <!-- Start Feature Section -->
        <div class="section-modal modal fade" id="feature-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                
                <div class="container">
                    <div class="row">
                        <div class="section-title text-center">
                            <h3>SYSTEM PROCESSES BY SMS </h3>
                           <!-- <p>Duis aute irure dolor in reprehenderit in voluptate</p>-->
                            <br />

                            <br />
                             <br />

                            <br />
                 
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="feature">
                                <i class="fa">1</i>
                                <div class="feature-content">
                                    <h4>Step 1</h4>
                                    <p> A Student can use any type of phone to vote for his or her contestant.</p>
                                </div>
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="feature">
                                <i class="fa">2</i>
                                <div class="feature-content">
                                    <h4>Step 2</h4>
                                    <p>A Student must first enter <b>(STUDENT ID SPACE NATIONAL IDENTIFICATION ID SPACE NAME OF CONTESTANT) </b> e.g 2b8gp23c G271paZs1b john</p>
                                </div>
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="feature">
                                <i class="fa">3</i>
                                <div class="feature-content">
                                   <h4>STEP 3</h4>
                                    <p>Stduent and national id's cannot vote twice in the system. If such incident happens, an sms "<b>THE STUDENT ID OR NATIONAL ID HAS ALREADY VOTED.</b>"</p>
                                </div>
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="feature">
                                <i class="fa">4</i>
                                <div class="feature-content">
                                    <h4>4 </h4>
                                    <p>If students wrongly enters studentID and nationalID details , a response message "<b> THE STUDENT_ID or NATIONAL_ID IS INCORRECT.KINDLY CHECK AND TRY AGAIN.</b>" is sent by sms to student.</p>
                                </div>
                            </div>
                        </div><!-- /.col-md-3 -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="feature">
                                <i class="fa">5</i>
                                <div class="feature-content">
                                    
                                    <h4>Step 5</h4>
                                    <p>If students correctly enters details , a response message "<b>THANK YOU FOR VOTIING FOR YOUR FAVOURITE CONTESTANT</b>" is sent by sms to student.</p>
                                </div>
                            </div>
                        </div><!-- /.col-md-3 -->
                        
                    </div><!-- /.row -->
                </div>
                
            </div>
        </div>
        <!-- End Feature Section -->
         
       <!-- *********************** why sms voting *****************************  -->

       <!-- Start About Us Section -->
        <div class="section-modal modal fade" id="about-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                
                <div class="container">
                    <div class="row">
                        <div class="section-title text-center">
                            <h3>About SMS Voting</h3>
                            <!--<p>Duis aute irure dolor in reprehenderit in voluptate</p> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="about-text">
                                <p>Voting is a widely spread and democratic way of making decisions. For centuries, Ghana has been using the popular paper-based voting system, which does not provide the desirable blend of accessibility and efficiency. Missing ballot papers, invalid votes and miscount are some of the challenges associated with the paper-based voting system. Electronic voting has become very popular, getting a lot attention and research for the past few years all over the world. The United State of America and some European countries like Italy and Germany have already implemented such electronic voting systems. These countries are enjoying a remarkable advantage over traditional paper-based voting</p>
                               
                            </div>
                        </div>
                    </div><!-- /.row -->
                    <div class="row">
                       
                        <div class="col-md-12">
                            <div class="custom-tab">
                        
                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                    <li class="active"><a href="#tab-1" role="tab" data-toggle="tab">Our Mission</a></li>
                                    <li><a href="#tab-2" role="tab" data-toggle="tab">Our Vission</a></li>
                                   <!-- <li><a href="#tab-3" role="tab" data-toggle="tab">Company History</a></li> -->
                                </ul>

                                <div class="tab-content">

                                    <div class="tab-pane active" id="tab-1">
                                        <p>This research proposes the use of mobile phones to facilitate communication and rapid access to information and their diffusion has reached a larger proportion of the population in a short period of time. When such a device is available why not use it for a time saving, cost effective, secured method of casting a vote. Mobile phone voting has the capability to increase the participation rate and reduce the amount errors in voting. In this documentation, we have designed and developed a mobile phone voting system using the SMS, which allows users to spontaneously vote using cellular network service providers. </p> <p> We developed the system using PHP and Java scripts for front end and Postgresql, kannel for SMS and nginx for back end. We conducted usability testing at University of Camerino (UNICAM) using stratified sampling and simple random sampling to get 51 participants. We collected data from the users by way of a self-completion questionnaire, which showed 93% of participants saying it is easy to use, 100% of participants saying it is easy to learn. The system was efficient, reliable, and accessible and secure as attested by participants.</p>
                                       
                                     </div>


                                    <div class="tab-pane" id="tab-2">
                                        <p>The voting process in today’s context is behind its time in respect of the usage of modern ICT as seen by experience. The voting process begins with persons manually going to an election office showing proof address and then a national identification card (Id) will be issued for getting the authentication during the actual process of voting at the polling booth/station. </p>
                                        <p>This process put a lot of stress on the voters and the delay in the presentation of results. In spite of this, we here by propose a novel Mobile voting technique for the first instance, with the hope that this Biometric based technology will erase the above issues. Our reported research focuses on the application of mobile technology with the use of short message send SMS</p>
                                    </div>


                                   

                                </div><!-- /.tab-content -->

                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- End About Us Section -->
        
         <!-- *********************** why sms voting *****************************  -->

       <!-- Start About Us Section -->
        <div class="section-modal modal fade" id="portfolio-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                
                <div class="container">
                    <div class="row">
                        <div class="section-title text-center">
                            <h3>Why this project</h3>
                            <!--<p>Duis aute irure dolor in reprehenderit in voluptate</p> -->
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="about-text">
                                <p>Our objective is to develop a platform which will allow schools, communities, organizations and a country to vote via SMS for a general election, referendum or selection of executives. We believe this platform will provide solutions to a lot electoral challenges in our country (Ghana).For the purpose of the course and the number of people working on this project, we will scale it down to the size of the University of Camerino and run a pilot test with the students in the class.</p>
                               
                            </div>
                        </div>

                         <div class="col-md-12 col-sm-12">
                            <div class="skill-shortcode">
                        
                                <!-- Progress Bar -->
                                <h3> Survey on manual and electronic voting </h3>
                                <div class="skill">
                                    <p>Manual</p>          
                                    <div class="progress">         
                                        <div class="progress-bar" role="progressbar"  data-percentage="60">
                                            <span class="progress-bar-span" >20%</span>
                                            <span class="sr-only">60% Complete</span>
                                        </div>
                                    </div>  
                                </div>

                                <!-- Progress Bar -->
                                <div class="skill">
                                    <p>SMS voting</p>          
                                    <div class="progress">         
                                        <div class="progress-bar" role="progressbar"  data-percentage="95">
                                            <span class="progress-bar-span" >80%</span>
                                            <span class="sr-only">95% Complete</span>
                                        </div>
                                    </div>  
                                </div>

                              

                            </div>
                        </div>
                    </div><!-- /.row -->
                    
                </div>
                
            </div>
        </div>
        <!-- End About Us Section -->

        <!-- team modal -->

        <!-- Start Team Member Section -->
        <div class="section-modal modal fade" id="team-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                
                <div class="container">
                    <div class="row">
                        <div class="section-title text-center">
                            <h3>Our Expert Team</h3>
                            <p>These are the project team who have made this project a realisation.</p>
                        </div>
                    </div>
                    <div class="row">                        
                        <div class="col-md-4 col-sm-3"  style="margin-left: 400px">
                            <div class="team-member">
                                <img src="images/team/nana.png" class="img-responsive" alt="" >
                                <div class="team-details">
                                    <h4>IRENE NANA SERWAA</h4>
                                    <div class="designation"></div>
                                    <ul style="text-align: center;">
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>                   

                        
                    </div><!-- /.row -->

                    <div class="row">
                        
                          <div class="col-md-4 col-sm-6"  style="margin-left: 250px">
                            <div class="team-member">
                                <img src="images/team/patrick.png" class="img-responsive" alt="">
                                <div class="team-details">
                                    <h4>PARICK KULOR</h4>
                                    <div class="designation"></div>
                                    <ul style="text-align: center;">
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6" style="margin-left: 50px">
                            <div class="team-member">
                                <img src="images/team/isaac.jpg" class="img-responsive" alt="">
                                <div class="team-details">
                                    <h4>ISAAC OHENE AYISI</h4>
                                    <div class="designation"></div>
                                    <ul style="text-align: center;">
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- End Team Member Section -->
        
       <!-- benefits -->

       <!-- Start About Us Section -->
        <div class="section-modal modal fade" id="service-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                
                <div class="container">
                    <div class="row">
                        <div class="section-title text-center">
                            <h3>Benefits of Our SMS Platform</h3>
                            <!--<p>Duis aute irure dolor in reprehenderit in voluptate</p>-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="about-text">
                                <p>The scope of this system covers a number of processes and functionalities that are available to front-end users and the back-end users and the voter of the system. The scope covers the functionalities below:</p>
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <ul>
                                            <li><i class="fa fa-check-square"></i>Login in his/her account with a Username and Password</li>
                                            <li><i class="fa fa-check-square"></i>Change Password</li>
                                            <li><i class="fa fa-check-square-o"></i>View Reports</li>
                                            <li><i class="fa fa-check-square-o"></i>Real time graphical Update</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <ul>
                                            <li><i class="fa fa-check-square"></i>Reset account of other users</li>
                                            <li><i class="fa fa-check-square"></i>Setup Voting</li>
                                            <li><i class="fa fa-check-square-o"></i>Manage Users(ADD,DELETE,UPDATE)</li>
                                            <li><i class="fa fa-check-square-o"></i>Download Reports</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <ul>
                                            <li><i class="fa fa-check-square"></i>Export Data to Excel,csv and pdf</li>
                                            <li><i class="fa fa-check-square"></i>Automatic sms reponse </li>
                                            <li><i class="fa fa-check-square-o"></i>Support all platform</li>
                                            <li><i class="fa fa-check-square-o"></i>Good Perfomance</li>
                                        </ul>
                                    </div>
                                </div><!-- /.row -->
                            </div>
                        </div>
                    </div><!-- /.row -->
                    
                </div>
                
            </div>
        </div>
        <!-- End About Us Section -->
        
    </body>
    
</html>