
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Online Voting</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">

     <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>


         <link href="../admin/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
          <link href="../admin/assets/global/plugins/bootstrap-toastr/toastr.css" rel="stylesheet" type="text/css" />
          <script src="../admin/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
     <script type="text/javascript">
      
     $(document).ready(function() {

     $("#getData").click(function(){
       $('#getName').val('isaac');
        $('#getNamee').val('isaac');
        });
     $("#getData1").click(function(){
       $('#getName').val('esther');
        $('#getNamee').val('esther');
        });
      
      $("#getData2").click(function(){
       $('#getName').val('john');
        $('#getNamee').val('john');
        });

       $("#getData3").click(function(){
       $('#getName').val('mary');
        $('#getNamee').val('mary');
        });


      });

     </script>
     

  </head>

  <body >

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">ONLINE VOTINE</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Home</a></li>
            <li><a href="../index.html">Main Menu</a></li>            
          </ul>
          <!--<form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>-->
        </div>
      </div>
    </nav>
          <br />
           <br />
            <br />
     
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="index.php">HOME<span class="sr-only">(current)</span></a></li>
            <li><a href="../index.html">Main Page</a></li>
           
          </ul>
          
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Candidates</h1>
           
          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3">
              <img src="../admin/Candidates/isaac.png" width="200" height="200" style="border-radius: 50%" alt="Generic placeholder thumbnail">
              <h4>ISAAC</h4>
             
              <span class="text-muted"><a class="btn btn-lg btn-primary"  id="getData"  data-toggle="modal" data-target="#myModalNorm">VOTE</a>
             
                </span>
            </div>
            <div class="col-xs-6 col-sm-3 ">
              <img src="../admin/Candidates/ruth.jpg" width="200" height="200" style="border-radius: 50%" alt="Generic placeholder thumbnail" >
              <h4>ESTHER</h4>
              <span class="text-muted"><a class="btn btn-lg btn-primary" id="getData1"   role="button" data-toggle="modal" data-target="#myModalNorm">VOTE</a></span>
            </div>
            <div class="col-xs-6 col-sm-3 ">
           <img src="../admin/Candidates/seth.jpg" width="200" height="200" style="border-radius: 50%" alt="Generic placeholder thumbnail">
              <h4>JOHN</h4>
             
              <span class="text-muted"><a class="btn btn-lg btn-primary" id="getData2"   role="button" data-toggle="modal" data-target="#myModalNorm">VOTE</a></span>
            </div>
            <div class="col-xs-6 col-sm-3 ">
              <img src="../admin/Candidates/mary.jpg" width="200" height="200" alt="Generic placeholder thumbnail" style="border-radius: 50%">
              <h4>MARY</h4>
              <span class="text-muted"><a class="btn btn-lg btn-primary"  id="getData3"   role="button" data-toggle="modal" data-target="#myModalNorm">VOTE</a></span>
            </div>
          </div>
             
    
        
        </div>
      </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                   VOTING ONLINE 
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
               <form id="votes" autocomplete="false" class="form-horizontal">
                                     <input type="hidden" name="opera" value="online_votes"/>
                                     <input type="hidden" name="phone_number" value="Online"/>
                                     <input type="hidden" name="nameCan" id="getNamee" />
                                        <div class="form-body">
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">CANDIDATE SELECTED
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="nameCann" id="getName" data-required="1" class="form-control" disabled="true" /> </div>
                                            </div>  
                                            <div class="form-group">
                                                <label class="control-label col-md-3">STUDENT ID
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="studentid" id="studID" data-required="1" class="form-control" /> </div>
                                            </div>  
                                            <div class="form-group">
                                                <label class="control-label col-md-3">NATIONAL ID
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="nationalid" id="nationalID" data-required="1" class="form-control" /> </div>
                                            </div>  
                                             <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal" id="modeclose">
                            Close
                </button>
                 
                  <button type="submit" class="btn btn-default">VOTE</button>
                </form>
                
                
            </div>
            
            <!-- Modal Footer -->
           
            </div>
        </div>
    </div>
</div>



 
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
     <script src="js/bootstrap.min.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="../admin/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
     
        <script src="../admin/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="../admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="../admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
   
    
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="js/holder.min.js"></script>
    <script src="js/vote.js"></script>
     <script src="../admin/assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    
  </body>
</html>
