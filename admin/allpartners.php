<?php

$page_title = "all partners";
$section = "partners";

include ("header.php");
?>
                        
        <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
            <div class="clearfix"></div>
            <!-- END DASHBOARD STATS 1-->
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-globe"></i><?php echo ucwords($page_title); ?></div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="all_partners">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" class="group-checkable" data-set="#all_partners .checkboxes" />
                                </th>
                                <th>Name</th>
                                <th>Business Type</th>
                                <th>Contact Pers.</th>
                                <th>Telephone</th>
                                <th>Created By</th>
                                <th>Date Created</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php             
    include ("footer.php");
?>