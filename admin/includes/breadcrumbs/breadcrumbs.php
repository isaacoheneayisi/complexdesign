                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                 
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="index.php">HOME</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span><?php echo strtoupper($section) ?></span>
                                <i class="fa fa-circle"></i>
                            </li>
                             <li>
                                <span><?php echo strtoupper($page_title) ?></span>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE TITLE-->
                    <!-- END PAGE HEADER-->