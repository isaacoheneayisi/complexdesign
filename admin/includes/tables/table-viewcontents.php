<?php
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
// Date Formatter
include('../dateClass.php');
// DB table to use
$table = 'mtechghana2.content_messages cm, subscription_service s';
// Table's primary key
$primaryKey = 'cm.id';
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array(
		'db'        => 'cm.id',
		'rb' 		=> 'service_name', 
		'dt'        => 0,
		'formatter' => function( $d, $row ) {
			return '<input type="checkbox" class="checkboxes" value="'. $d .'" />';
		}
	),
	array( 
		'db' => 's.service_name',
		'rb' => 'service_name',  
		'dt' => 1 
	),
	array( 
		'db' => 'cm.description',
		'rb' => 'description',  
		'dt' => 2 
	),
	array(
		'db' => 'cm.pubdate',
		'rb' => 'pubdate', 
		'dt' => 3,
		'formatter' => function( $d, $row ) {
			$date  = date_create($d);
            return date_format($date, 'd-m-Y');
		}
	),
	array(
		'db' => 'cm.lastupdate',
		'rb' => 'lastupdate', 
		'dt' => 4,
		'formatter' => function( $d, $row ) {
			$formatDate = new Date_Difference;
			return $formatDate->getStringResolved($d);
		}
	)
);

$cwhere = 'cm.cat_id = s.id';

// SQL server connection information
include_once('tables-dbconn.php'); 

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
require( 'ssp.class.php' );
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $cwhere )
);
