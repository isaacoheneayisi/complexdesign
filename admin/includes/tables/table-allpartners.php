<?php
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
// DB table to use
$table = 'partners';
// Table's primary key
$primaryKey = 'id';
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array(
		'db'        => 'id',
		'dt'        => 0,
		'formatter' => function( $d, $row ) {
			return '<input type="checkbox" class="checkboxes" value="'. $d .'" />';
		}
	),
	array( 'db' => 'partner_name', 'dt' => 1 ),
	array( 'db' => 'partner_business', 'dt' => 2 ),
	array( 'db' => 'contact_person', 'dt' => 3 ),
	array( 'db' => 'telephone', 'dt' => 4 ),
	array( 'db' => 'created_by', 'dt' => 5 ),
	array(
		'db' => 'datecreated',
		'dt' => 6,
		'formatter' => function( $d, $row ) {
			$date  = date_create($d);
            return date_format($date, 'd-m-Y');
		}
	),
	array(
		'db'        => 'id',
		'dt'        => 7,
		'formatter' => function( $d, $row ) {
			return '<a type="button" class="btn green-meadow" href='. $d .'>View Details</a>';
		}
	)
);

// SQL server connection information
include_once('tables-dbconn.php'); 

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
require( 'ssp.class.php' );
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);
