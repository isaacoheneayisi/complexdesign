<?php
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
// DB table to use
$table = 'viewservices';
// Table's primary key
$primaryKey = 'id';
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => 'service_name', 'dt' => 0 ),
	array( 'db' => 'service_shortcode', 'dt' => 1 ),
	array( 'db' => 'service_category', 'dt' => 2 ),
	array( 'db' => 'service_description', 'dt' => 3 ),
	array( 'db' => 'service_network', 'dt' => 4 ),
	array( 'db' => 'login', 'dt' => 5 ),
	array(
		'db' => 'date_created',
		'dt' => 6,
		'formatter' => function( $d, $row ) {
			$date  = date_create($d);
            return date_format($date, 'd-m-Y');
		}
	),
	array(
		'db'        => 'id',
		'dt'        => 7,
		'formatter' => function( $d, $row ) {
			return '<a type="button" class="btn green-meadow modal" href="modal/updatecontent.php"  >View Details</a>';

		}
	)
);

// SQL server connection information
include_once('tables-dbconn.php'); 

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
require( 'ssp.class.php' );
echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);
