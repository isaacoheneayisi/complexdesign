
<?php
include_once('includes/config.php');
include_once('includes/func.php');
$page_title = "Home";
$section = "SMS VOTING "; 
//$success_sms = get_total_success();
//$all_sms = get_total_sms();
//$sms_fail =get_total_fail();
//$success_rate =0;
//if($all_sms > 0)$success_rate = round(($success_sms/$all_sms)* 100); 

include ("header.php"); 



?>
<script type="text/javascript">
    setTimeout(function () { 
      location.reload();
    }, 60 * 1000);
</script>

<style type="text/css">
    
    .img-circle {
    border-radius: 50%;
}

</style>
        <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
                    
            <div class="clearfix"></div>
            <!-- END DASHBOARD STATS 1-->
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <!-- <input type="hidden" id="chartdata" value="<?php echo get_data_for_graph(); ?>" -->
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-globe"></i>All <?php echo ucwords($section) ?> </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                <div class="visual">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="0"><?php echo expected_votes(); ?></span>
                                    </div>
                                    <div class="desc"> Total Expected Votes </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                <div class="visual">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="0"><?php echo successful_votes(); ?></span> </div>
                                    <div class="desc"> Successful votes  </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                <div class="visual">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                       <!-- <span data-counter="counterup" data-value="0"><?php  //echo $sms_fail; ?></span>-->
                                       <span data-counter="counterup" data-value="0"><?php  echo cancelled_votes(); ?></span>
                                    </div>
                                    <div class="desc"> Cancelled votes  </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                                <div class="visual">
                                    <i class="fa fa-bar-chart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">  
                                        <span data-counter="counterup" data-value="0"></span><?php echo number_format((float)(successful_votes()/expected_votes() *100),2,'.',''); ?>% </div>
                                    <div class="desc"> Succes Rate </div>
                                </div>
                            </a>
                        </div>
                    </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                    
                      <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-globe"></i> Candidates </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                         
                            <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                <div class="visual">

                                    <i class="fa fa-envelope"></i>

                                </div>

                                <?php

                                 $Total_successful_votes= isaac_votes()+john_votes()+mary_votes()+esther_votes();

                                ?>

                                <div class="details">
                                <div><div style="float:left;padding-left: 10px"><b style="font-size: 25px;color: #FFF">ISAAC </b><br /> <b style="font-size: 40px;color: #FFF"><?php  echo number_format((float)(isaac_votes()/$Total_successful_votes*100),2,'.',''); ?>%</b> </div><img class="img-circle" style="width: 120px; height: 120px" src="candidates/isaac.png"></div>
                                    <div class="number">
                                        <span data-counter="counterup" data-value="0"><?php //echo $all_sms; ?></span>
                                    </div>
                              
                                    
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                <div class="visual">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <div class="details">
                                <div><div style="float:left"><b style="font-size: 25px;color: #FFF">JOHN </b><br />  <b style="font-size: 40px;color: #FFF"> <?php  echo number_format((float)(john_votes()/$Total_successful_votes *100),2,'.',''); ?>%</b> </div><img class="img-circle" style="width: 120px; height: 120px" src="candidates/seth.jpg"></div>
                                    <div class="number">
                                        <span data-counter="counterup" data-value="0"><?php //echo $success_sms; ?></span> </div>
                                    <div class="desc" > ISAAC </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                <div class="visual">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="details">
                                <div><div style="float:left"> <b style="font-size: 25px;color: #FFF">MARY</b> <br /> <b style="font-size: 40px;color: #FFF"><?php  echo number_format((float)(mary_votes()/$Total_successful_votes *100),2,'.',''); ?>%</b> </div><img class="img-circle" style="width: 120px; height: 120px" src="candidates/mary.jpg"></div>
                                    <div class="number">
                                       <!-- <span data-counter="counterup" data-value="0"><?php  //echo $sms_fail; ?></span>-->
                                       <span data-counter="counterup" data-value="0"><?php  echo ""; ?></span>
                                    </div>
                                    <div class="desc"> MARY  </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                                <div class="visual">
                                    <i class="fa fa-bar-chart"></i>
                                </div>
                                <div class="details">
                                <div><div style="float:left"> <b style="font-size: 25px;color: #FFF;padding-left: 2px">ESTHER </b> <br /> <b style="font-size: 40px;color: #FFF"><?php  echo number_format((float)(esther_votes()/$Total_successful_votes*100),2,'.',''); ?>%</b> </div><img class="img-circle" style="width: 120px; height: 120px" src="candidates/ruth.jpg"></div>
                                    <div class="number">
                                        <span data-counter="counterup" data-value="0"></span><?php //echo $success_rate ?></div>
                                    <div class="desc"> ESTHER </div>
                                </div>
                            </a>
                        </div>
                    </div>
                        </div>
                    </div>

                    <div id="chartdiv"></div>
                    



            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php   
    include ("footer.php");
?>