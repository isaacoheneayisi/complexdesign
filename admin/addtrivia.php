
                      <?php   
					  
					        include ("header.php");
					   ?>
                        
                          <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
                 
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- Main content page-->
                         <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN VALIDATION STATES-->
                            <div class="portlet light portlet-fit portlet-form bordered">
                                
                                <div class="portlet-body">
                                    <!-- BEGIN FORM-->
									 <div>
                                                <label class="col-md-12" align="center" style="color:red;padding-left:20px">
                                                    NB: Excel column format: Question | Answer | Answer Position
                                                </label>
                                              
                                            </div>	
                                    <form action="#" id="form_sample_3" class="form-horizontal">
                                        <input type="hidden" name="opera" value="addtrivia"/>
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                                                                    
                                           				   
										   
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Trivia Service
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <select class="form-control select2me" name="options2">
                                                        <option value="">Trivia Service</option>
                                                        <option value="Option 1">4syte</option>
                                                        <option value="Option 2">vodafoneIcon</option>
                                                        <option value="Option 3">textnwin</option>
                                                        
                                                    </select>
                                                </div>
                                            </div>
											
                                           <div class="form-group last">
                                                <label class="control-label col-md-3">Question*
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                     <textarea class="form-control" rows="6"  name="sms_content"></textarea>                                    
                                                </div>
                                            </div>	
											<div class="form-group last">
                                                <label class="control-label col-md-3">Answer*
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-5">
                                                     <textarea class="form-control" rows="6"  name="sms_content"></textarea>                                    
                                                </div>
                                            </div>	
                                                                      
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Answer Position*
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="name" data-required="1" class="form-control" /> </div>
                                            </div>                                         
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Trivia Group (Specify Trivia Group)
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="name" data-required="1" class="form-control" /> </div>
                                            </div>                                         
                                        
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">Submit</button>
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                                <!-- END VALIDATION STATES-->
                            </div>
                        </div>
                    </div>
                </div>
                            <!-- end main content page-->
                      
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
      
	                      <?php   
					  
					        include ("footer.php");
					   ?>