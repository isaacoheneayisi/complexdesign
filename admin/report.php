
                      <?php   
					        include_once('includes/config.php');
                            include_once('includes/func.php');
					        
                            $page_title = "Report";
                            $section = "fire sms"; 
                            include ("header.php");
					   ?>
                        
                          <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
                 
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">

                                        <form class="form-inline" role="form" id="reportform">                                        
                                          <div class="form-group">
                                            <label for="sdate" >Start Date:</label>
                                            <input type="text"  class="form-control date-picker" data-date-format="dd/mm/yyyy" name="sdate" id="sdate">
                                          </div>
                                          <div class="form-group">
                                            <label for="edate">End Date:</label>
                                            <input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" name="edate" id="edate">
                                          </div>                                          
                                          <button type="submit" class="btn btn-primary">Generate</button>
                                        </form>    

                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                <br/>
                                <br/>
<table width="100%"String class="display"String id="report_table"String cellspacing="0"String>

        <thead>

            <tr>

                <th>Date</th>

                <th>Total SMS</th>

                <th>Delivered</th>

                <th>Failed</th>
                <th>Success Rate</th>
                <th>Cost</th>

            </tr>

        </thead>

        <tfoot>

            <tr>

               <th>Total</th>

                <th></th>

                <th></th>

                <th></th>
                <th></th>
                <th></th>

            </tr>

        </tfoot>

    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                      
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
      
	                      <?php   
					  
					        include ("footer.php");
					   ?>