
                      <?php                        
					     include_once('includes/config.php');
                         include_once('includes/func.php');
 				        
                         $id = $_REQUEST['id'];
                        $row = pg_fetch_array(get_partners($id));
                        $org = $row['org'];
                        $firstname = $row['firstname'];
                        $lastname = $row['lastname'];
                        $email = $row['email'];
                        $phone1 = $row['phone1'];
                        $phone2 = $row['phone2'];
                        $accounttype = $row['accounttype'];
                        $costpersms = $row['costpersms'];
                        $active = $row['active'];
                        $api = $row['apikey'];
					   ?>

                        
                          <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                
                 
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                            <!-- Main content page-->
                         <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN VALIDATION STATES-->
                            <div class="portlet light portlet-fit portlet-form bordered">
                                
                                <div class="portlet-body">
                                    <!-- BEGIN FORM-->
                                    <form id="update_partner" autocomplete="false" class="form-horizontal">
                                    <input type="hidden" name="opera" value="updatepartner"/>
                                    <input type="hidden" name="apikey" class="apikey" value="<?php echo $api; ?>">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">COMPANY NAME
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="company_name" id="company_name" data-required="1" class="form-control" value="<?php echo $org; ?>" /> </div>
                                            </div>       
                                               <div class="form-group">
                                                <label class="control-label col-md-3">CONTACT PERSON'S FIRST NAME
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="fname" id="fname" data-required="1" class="form-control"  value="<?php echo $firstname; ?>"/> </div>
                                            </div>   

											  <div class="form-group">
                                                <label class="control-label col-md-3">CONTACT PERSON'S LAST NAME
                                                    <span class="required"> </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="lname" id="lname" data-required="1" class="form-control"  value="<?php echo $lastname; ?>"/> </div>
                                            </div>     
											 <div class="form-group">
                                                <label class="control-label col-md-3">CONTACT EMAIL
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="email" name="email" id="email" data-required="1" class="form-control" value="<?php echo $email; ?>"/> </div>
                                            </div>  
                                            <div class="form-group">
                                                <label class="control-label col-md-3">PHONE 1
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="tel" name="phone1" id="phone1" data-required="1" class="form-control" value="<?php echo $phone1; ?>"/> </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">PHONE 2
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="tel" name="phone2" id="phone2" data-required="1" class="form-control" value="<?php echo $phone2; ?>"/> </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">SMS SERVICE
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <select class="form-control" id="account_type" name="account_type" required>
                                                        <option <?php if($accounttype == 'pay'){ ?> selected="selected" <?php } ?> value="pay">Post Paid</option>
                                                        <optionn<?php if($accounttype == 'unlimited'){ ?> selected="selected" <?php } ?> value="unlimited">Pay Monthly</option>                                                  
                                                        
                                                    </select>
                                                </div>
                                            </div>
											 
                                             <div class="form-group">
                                                <label class="control-label col-md-3">COST/SMS
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" name="cost" id="cost" data-required="1" class="form-control" value="<?php echo $costpersms; ?>"/> </div>
                                            </div>

											  

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Active
                                                    <span class="required">  </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="checkbox" name="active" <?php if($active == '1'){echo 'checked';}else{echo '';} ?> /> </div>
                                            </div>  

											                           
                                           
                                        <div class="form-actions">

                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">Submit</button>
                                                    <button type="button" class="btn red deleteaccount">Delete</button>
                                                    <button type="button" class="btn default closemodal" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM
                                </div>
                                <!-- END VALIDATION STATES-->
                            </div>
                        </div>
                    </div>
                </div>
                            <!-- end main content page-->
                      
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
