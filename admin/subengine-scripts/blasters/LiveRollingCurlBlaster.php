<?php

/*
  Authored by Josh Fraser (www.joshfraser.com)
  Released under Apache License 2.0

  Maintained by Alexander Makarov, http://rmcreative.ru/

  $Id$
 */

/**
 * Class that represent a single curl request
 */
class RollingCurlRequest {

    public $url = false;
    public $method = 'GET';
    public $post_data = null;
    public $headers = null;
    public $options = null;

    /**
     * @param string $url
     * @param string $method
     * @param  $post_data
     * @param  $headers
     * @param  $options
     * @return void
     */
    function __construct($url, $method = "GET", $post_data = null, $headers = null, $options = null) {
        $this->url = $url;
        $this->method = $method;
        $this->post_data = $post_data;
        $this->headers = $headers;
        $this->options = $options;
    }

    /**
     * @return void
     */
    public function __destruct() {
        unset($this->url, $this->method, $this->post_data, $this->headers, $this->options);
    }

}

/**
 * RollingCurl custom exception
 */
class RollingCurlException extends Exception {
    
}

/**
 * Class that holds a rolling queue of curl requests.
 *
 * @throws RollingCurlException
 */
class RollingCurl {

    /**
     * @var int
     *
     * Window size is the max number of simultaneous connections allowed.
     *
     * REMEMBER TO RESPECT THE SERVERS:
     * Sending too many requests at one time can easily be perceived
     * as a DOS attack. Increase this window_size if you are making requests
     * to multiple servers or have permission from the receving server admins.
     */
    private $window_size = 5;

    /**
     * @var float
     *
     * Timeout is the timeout used for curl_multi_select.
     */
    private $timeout = 10;
    #private $timeout = 2;

    /**
     * @var string|array
     *
     * Callback function to be applied to each result.
     */
    private $callback;

    /**
     * @var array
     *
     * Set your base options that you want to be used with EVERY request.
     */
    protected $options = array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_CONNECTTIMEOUT => 30,
        CURLOPT_TIMEOUT => 30
    );

    /**
     * @var array
     */
    private $headers = array();

    /**
     * @var Request[]
     *
     * The request queue
     */
    private $requests = array();

    #DEMOLA MODIFICATION
    private $noofrequests = 0;
    private $requestscompleted = 0;

    /**
     * @var RequestMap[]
     *
     * Maps handles to request indexes
     */
    private $requestMap = array();
    public $queue_name;

    /**
     * @param  $callback
     * Callback function to be applied to each result.
     *
     * Can be specified as 'my_callback_function'
     * or array($object, 'my_callback_method').
     *
     * Function should take three parameters: $response, $info, $request.
     * $response is response body, $info is additional curl info.
     * $request is the original request
     *
     * @return void
     */
    function __construct($callback = null) {
        $this->callback = $callback;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function __get($name) {
        return (isset($this->{$name})) ? $this->{$name} : null;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return bool
     */
    public function __set($name, $value) {
        // append the base options & headers
        if ($name == "options" || $name == "headers") {
            $this->{$name} = $value + $this->{$name};
        } else {
            $this->{$name} = $value;
        }
        return true;
    }

    /**
     * Add a request to the request queue
     *
     * @param Request $request
     * @return bool
     */
    public function add($request) {
        $this->requests[] = $request;
        return true;
    }

    /**
     * Create new Request and add it to the request queue
     *
     * @param string $url
     * @param string $method
     * @param  $post_data
     * @param  $headers
     * @param  $options
     * @return bool
     */
    public function request($url, $method = "GET", $post_data = null, $headers = null, $options = null) {
        $this->requests[] = new RollingCurlRequest($url, $method, $post_data, $headers, $options);
        return true;
    }

    /**
     * Perform GET request
     *
     * @param string $url
     * @param  $headers
     * @param  $options
     * @return bool
     */
    public function get($url, $headers = null, $options = null) {
        return $this->request($url, "GET", null, $headers, $options);
    }

    /**
     * Perform POST request
     *
     * @param string $url
     * @param  $post_data
     * @param  $headers
     * @param  $options
     * @return bool
     */
    public function post($url, $post_data = null, $headers = null, $options = null) {
        return $this->request($url, "POST", $post_data, $headers, $options);
    }

    /**
     * Execute processing
     *
     * @param int $window_size Max number of simultaneous connections
     * @return string|bool
     */
    public function execute($window_size = null) {
        // rolling curl window must always be greater than 1
        global $result;
        $rows = pg_num_rows($result);
        debug("XXX BROADCASTING $rows CALLS (Connections:  {$this->window_size}) XXX");
        #echo "SIZE:".sizeof($this->requests);
        $this->noofrequests = $rows;
        if ($this->noofrequests == 1) {
            return $this->single_curl();
        } else {
            // start the rolling curl. window_size is the max number of simultaneous connections
            return $this->rolling_curl($window_size);
        }
    }

    /**
     * Performs a single curl request
     *
     * @access private
     * @return string
     */

    private function single_curl() {
		global $result;
		
		$url_log_file_name = raw_url_log_file_name(date("Ymd"), BLASTER_TYPE, BLAST_SEQ);
        $fp = fopen(LOCATION . $url_log_file_name . ".log", 'a');

        debug("\n-+-+-+-+-+- \nSTARTING 1 Call (Connections:  1)");
		
		$row = pg_fetch_assoc($result);

        fwrite($fp, "{$url}\n");
		
        $ch = curl_init();
        $request = array_shift($this->requests);
        #$options = $this->get_options($request);
		$url = sendsms($row);
		
		$options = array(
                CURLOPT_URL => $url,
                CURLOPT_HEADER => false,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CONNECTTIMEOUT => 30,
                CURLOPT_TIMEOUT => 30
            );
        curl_setopt_array($ch, $options);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);

        // it's not neccesary to set a callback for one-off requests
        if ($this->callback) {
            #Seems to not be used
            #echo "call 1\n";
            $callback = $this->callback;
            if (is_callable($this->callback)) {
                call_user_func($callback, $output, $info, $request);
            }
        }
        else
            return $output;
        return true;
    }


    /**
     * Performs multiple curl requests
     *
     * @access private
     * @throws RollingCurlException
     * @param int $window_size Max number of simultaneous connections
     * @return bool
     */
    private function rolling_curl($window_size = null) {
        global $result;
        $rows = pg_num_rows($result);
        
        
        #EXPERIMENTAL SAFETY MEASURE START
        $temp_requestscompleted = 0;
        #EXPERIMENTAL SAFETY MEASURE END


        $url_log_file_name = raw_url_log_file_name(date("Ymd"), BLASTER_TYPE, BLAST_SEQ);
        $fp = fopen(LOCATION . $url_log_file_name . ".log", 'a');

        debug("\n-+-+-+-+-+- \nSTARTING $rows Calls (Connections:  {$this->window_size})");
        if ($window_size)
            $this->window_size = $window_size;

        // make sure the rolling window isn't greater than the # of urls
        if ($this->noofrequests < $this->window_size)
            $this->window_size = $this->noofrequests;

        if ($this->window_size < 2) {
            throw new RollingCurlException("Window size must be greater than 1");
        }

        $master = curl_multi_init();

        // start the first batch of requests
        for ($i = 0; $i < $this->window_size; $i++) {
            $ch = curl_init();

            #Form URL
            $row = pg_fetch_assoc($result);

            $url = sendsms($row);
            $options = array(
                #CURLOPT_URL => 'http://localhost/test/ping.php?'.'shortcode='.$row['shortcode'].'message='.$row['message'],
                #CURLOPT_URL => 'http://10.40.42.223/test/a.php?sleepa=5&'.'shortcode='.$row['shortcode'].'message='.$row['message'],
                CURLOPT_URL => $url,
                CURLOPT_HEADER => false,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_CONNECTTIMEOUT => 30,
                CURLOPT_TIMEOUT => 30
            );

            fwrite($fp, "{$url}\n");
            /*
              $options = array(
              CURLOPT_RETURNTRANSFER => true,         // return web page
              CURLOPT_HEADER         => false,        // don't return headers
              CURLOPT_FOLLOWLOCATION => true,         // follow redirects
              CURLOPT_ENCODING       => "",           // handle all encodings
              CURLOPT_USERAGENT      => "spider",     // who am i
              CURLOPT_AUTOREFERER    => true,         // set referer on redirect
              CURLOPT_CONNECTTIMEOUT => 120,          // timeout on connect
              CURLOPT_TIMEOUT        => 120,          // timeout on response
              CURLOPT_MAXREDIRS      => 10,           // stop after 10 redirects
              CURLOPT_POST            => 1,            // i am sending post data
              CURLOPT_POSTFIELDS     => $curl_data,    // this are my post vars
              CURLOPT_SSL_VERIFYHOST => 0,            // don't verify ssl
              CURLOPT_SSL_VERIFYPEER => false,        //
              CURLOPT_VERBOSE        => 1                //
              );
             */

            curl_setopt_array($ch, $options);
            curl_multi_add_handle($master, $ch);
            #echo "\n NO OF REQUESTS:".$this->noofrequests." REG COMPLETED".$this->requestscompleted;
            // Add to our request Maps
            $key = (string) $ch;
            $this->requestMap[$key] = $i;
        }

        do {
            while (($execrun = curl_multi_exec($master, $running)) == CURLM_CALL_MULTI_PERFORM) {
                if ($execrun != CURLM_OK) {
                    break;
                }
            }
#/*				
            // a request was just completed -- find out which one
            while ($done = curl_multi_info_read($master)) {

                // get the info and content returned on the request
                $info = curl_getinfo($done['handle']);
                $output = curl_multi_getcontent($done['handle']);

                if ($this->requestscompleted >= $this->noofrequests) {
                    break; //MAY NOT BE THE BEST PROCEDURE
                } else {
                    // send the return values to the callback function.
                    $callback = $this->callback;
                    if (is_callable($callback)) {
                        #echo "call 2\n";
                        $key = (string) $done['handle'];
                        $request = $this->requests[$this->requestMap[$key]];
                        unset($this->requestMap[$key]);
                        call_user_func($callback, $output, $info, $request, array('no_of_req' => $this->noofrequests, 'req_complete' => $this->requestscompleted));
                        $this->requestscompleted++;
                    }
                }

                // start a new request (it's important to do this before removing the old one)
                if ($this->is_queue_empty() !== TRUE) {
                    $ch = curl_init();

                    #Form URL
                    $row = pg_fetch_assoc($result);
                    if (strlen($row['msisdn']) > 5) {
                        $url = sendsms($row);
                        $options = array(
                            #CURLOPT_URL => 'http://localhost/test/ping.php?'.'shortcode='.$row['shortcode'].'message='.$row['message'],
                            #CURLOPT_URL => 'http://10.40.42.223/test/a.php?sleepa=5&'.'shortcode='.$row['shortcode'].'message='.$row['message'],
                            CURLOPT_URL => $url,
                            CURLOPT_HEADER => false,
                            CURLOPT_SSL_VERIFYPEER => 0,
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_CONNECTTIMEOUT => 30,
                            CURLOPT_TIMEOUT => 30
                        );

                        fwrite($fp, "{$url}\n");

                        curl_setopt_array($ch, $options);
                        curl_multi_add_handle($master, $ch);
                        #echo "\n NO OF REQUESTS:".$this->noofrequests." REG COMPLETED".$this->requestscompleted;
                        #$this->requestscompleted++;
                        // Add to our request Maps
                        $key = (string) $ch;
                        $this->requestMap[$key] = $i;
                        $i++;
                    }
                }

                // remove the curl handle that just completed
                curl_multi_remove_handle($master, $done['handle']);
            }
#*/ 
            #sleep(10);
            // Block for data in / output; error handling is done by curl_multi_exec
            if ($running) {
                $rr = curl_multi_select($master, $this->timeout);
            }

            #DEMOLA ADDED TO ENSURE PROCESS NEVER STOPS IN CASE THE URL RETURNS TO QUICKLY
            if ($running == 0) {
                debug("RUNNING STOPPED!!!!!!!\n");
                debug("Requests Completed: " . $this->requestscompleted . ", No. of Requests:" . $this->noofrequests);
                if ($this->requestscompleted + 1 < $this->noofrequests) {
                    debug(".....AND PROCESS NOT FINISHED!!!!!!!\n FORCING TO CONTINUE\n");
                    $running = TRUE;
                    
                    #EXPERIMENTAL SAFETY MEASURE START					

					if($temp_requestscompleted != $this->requestscompleted){
						#NEW AMOUNT, ADD 1
						$temp_requestscompleted = $this->requestscompleted;
						$temp_requestscompleted_count[$temp_requestscompleted]++;
						
					}else{
						#LOOPING SITUATION
						$temp_requestscompleted_count[$temp_requestscompleted]++;
						if($temp_requestscompleted_count[$temp_requestscompleted] >= ($this->window_size * 2) ){
							$running = FALSE;
						}
					}


                    #EXPERIMENTAL SAFETY MEASURE END
                    
                    
                }
            }
        } while ($running);
        curl_multi_close($master);
        return true;
    }

    /**
     * Helper function to set up a new request by setting the appropriate options
     *
     * @access private
     * @param Request $request
     * @return array
     */
    private function get_options($request) {

        // options for this entire curl object
        $options = $this->__get('options');
        if (ini_get('safe_mode') == 'Off' || !ini_get('safe_mode')) {
            $options[CURLOPT_FOLLOWLOCATION] = 1;
            $options[CURLOPT_MAXREDIRS] = 5;
        }
        $headers = $this->__get('headers');

        // append custom options for this specific request
        if ($request->options) {
            $options = $request->options + $options;
        }

        // set the request URL
        $options[CURLOPT_URL] = $request->url;

        // posting data w/ this request?
        if ($request->post_data) {
            $options[CURLOPT_POST] = 1;
            $options[CURLOPT_POSTFIELDS] = $request->post_data;
        }
        if ($headers) {
            $options[CURLOPT_HEADER] = 0;
            $options[CURLOPT_HTTPHEADER] = $headers;
        }

        return $options;
    }

    private function is_queue_empty() {
        if ($this->requestscompleted >= $this->noofrequests) {
            #Queue Empty
            return true;
        } else {
            #Queue not Empty
            return false;
        }
    }

    /**
     * @return void
     */
    public function __destruct() {
        unset($this->window_size, $this->callback, $this->options, $this->headers, $this->requests);
    }

}

/*

  A key concept of curl_multi is that curl_multi_exec may return before all the sub-requests are completed. This contrasts to curl_exec, which (normally) returns only after the request has been fully processed. Therefore, you must repeatedly call curl_multi_exec until it returns a value indicating that processing has completed. Handling this behaviour requires more code.

  The example code in the documentation is simple but inefficient, because it uses busy waiting/polling while the parallel requests are running. The vastly more efficient method is to use curl_multi_select, which blocks (efficiently waits) and only returns when there is more data to process. The function curl_multi_select internally calls curl_multi_fdset followed by a select, which you can see in the curl_multi source code. See the libcurl-multi documentation for more information on what curl_multi_fdset does. Remember that curl_multi_exec is equivalent to the curl_multi_perform in libcurl-multi.
  ---------------------------------------------------------------------

  daemon runs indefinitely
  checks that size is greater than 0
  if then run script else wait for 2 seconds and check again


  Script:

  dont use for loops. only while loops.

 */
?>

