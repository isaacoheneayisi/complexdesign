<?php
ini_set('memory_limit', '-1');
/* 
 * This file will read all the data from a file in batches and writes to DB
*/
require_once 'universal.php';

class xdlr_persist extends universalclass {

    public $from;
    public $to;
    public $msg;

    
    public $smsc_id;
    public $smsc;
    public $type;

    public $live; 	//persist straight to DB or log to file
    public $writetype;	//write to file using linux or php

    private $store;     //where arrays will be stored for "copy from" to DB
    private $trash;     //where bad arrays will be stored for copy later

    private $active_file;   //file being processed
    private $status;    //if true then copy is successful and all files can be cleaned_up

    

    const type = '0';
    const live = FALSE;
    const writetype = 0; //0 = php (default), 1 = linux


    public function xdlr_persist() {

        $this->set_all_options();
        $this->start_processing();
        $this->validate();
        $this->persist_to_db();
        $this->finish_processing();

    }

    private function persist_to_db() {
        var_dump($this->store);
        $connect = $this->connect_to_db();
        if($connect && is_array($this->store)) {

            #CREATE A TEMP TABLE THAT WILL RECEIVE THE COPY RESULTS
            $create = 'CREATE TEMP TABLE _temp_dlr
                        (
                            "date" timestamp,
                            "from" character varying,
                            "to" character varying,
                            msg character varying,
                            service_id integer,
                            mno integer,
                            "type" integer,
                            content_id integer,
                            msg_id character varying,
                            http_status character varying,
                            blaster_id integer,
                            blast_seq integer,
                            dlr_smsc character varying,
                            dlr_submit timestamp,
                            dlr_done timestamp,
                            dlr_status character varying,
                            dlr_duration integer,
                            sub_source character varying
                        )
                        WITH (
                          OIDS=FALSE
                        );';
            pg_query($create);

            #RUN COPY COMMAND
            $copy = @pg_copy_from($connect, '_temp_dlr', $this->store,"\t",null);
            if($copy) {
                echo 'PG COPY Successfull ('.count($this->store)." items copied)\n";
            }else {
                echo 'PG COPY Error Occured: '.pg_last_error()."\n";
            }

        } else {
            echo "Problem with connection or array store. \n";
        }
		
        if($copy) {
            #TRANSFER COPIED TEMP TABLE TO MAIN DLR TABLE. TABLE WILL BE DESTROYED AFTER INSTANCE IS DESTROYED
            $query = @pg_query('insert into dlr("date","from","to",msg,service_id,mno,"type",content_id, msg_id, http_status,blaster_id, blast_seq, dlr_smsc, dlr_submit, dlr_done, dlr_status, dlr_duration,sub_source) (select "date","from","to",msg,service_id,mno,"type",content_id, msg_id, http_status,blaster_id, blast_seq, dlr_smsc, dlr_submit, dlr_done, dlr_status, dlr_duration,sub_source from _temp_dlr);');
        }
        #IF COPY PROCESS COMPLETES FULLY
        if($query && $copy) {
            $this->status = 1;
        }else {
            $this->status = 0;
        }
    }

    private function validate() {
        $handle = @fopen($this->processing_dir.$this->active_file, "r") or die("Couldn't get handle ".$this->processing_dir.$this->active_file."\n");
        if ($handle) {
            echo "reading file (".$this->processing_dir.$this->active_file.")\n";
            while (!feof($handle)) {
                set_time_limit(60);
                #$buffer = fgetcsv($handle, 4096,"\t","dddddd");
                $buffer = fgets($handle, 4096);

                #IF LINE IS NOT EMPTY THEN CONTINUE
                if(!empty($buffer)) {

                    #SPLIT LINE INTO COLUMNS AND PUT IN ARRAY
                    $array_of_columns = @split("\t", $buffer);
                    #print_r($array_of_columns);

                    if(count($array_of_columns) == 17) {
                        #FILE HAS RIGHT AMOUNT OF COLUMNS, STORE IN GOOD LIST
                        #echo "GOOD COLS\n";
			$buffer = str_replace("\n",'',$buffer);
			$buffer = $buffer."\t";
                        $this->store[] = $buffer; #implode(",", $array_of_columns);
                    }else if(count($array_of_columns) == 18) {
                        #FILE HAS RIGHT AMOUNT OF COLUMNS, STORE IN GOOD LIST
                        #echo "GOOD COLS\n";
                        $this->store[] = $buffer; #implode(",", $array_of_columns);
                    }else {
                        #FILE HAS WRONG AMOUNT OF COLUMNS, STORE IN BAD LIST
                        #echo "BAD COLS\n";
			echo "BAD ROW!";
			echo count($array_of_columns);
			echo "\n";
                        $this->trash[] = $buffer;
                    }

                }else {

                    echo "LINE EMPTY!\n";
                }
            }
            fclose($handle);
        }
    }

    private function start_processing() {
        echo "=====================START=====================\n";
		$now = date('Y-m-d H:i:s');
        echo $now."\n";
        echo "-----------------------------------------------\n";
		
        #FILE WILL BE MOVED TO PROCESSING AREA
        $destination = $this->processing_dir;
        #$destination = "csv\\processing\\";
        $filename = $this->file;
        $now = date('Ymd_His');
        $new_filename = $now.'_dlr.csv';
        $this->active_file = $new_filename;
		
		if(file_exists($filename)){
        $rename_n_move = $this->move_files($this->file, $destination.$new_filename);
        var_dump($rename_n_move);
		}else{
        echo "no file to persist\n";
		$now = date('Y-m-d H:i:s');

        echo "-----------------------------------------------\n";
        echo $now."\n";
        echo "======================END======================\n";
		exit;
		}
    }

    private function finish_processing() {
        #FILE WILL BE MOVED TO FINAL AREA / CLEAN UP
        echo "TRASH ".count($this->trash)." items \n";
        #print_r($this->trash);
        echo "STORE ".count($this->store)." items \n";
        #print_r($this->store);

        if(!empty($this->status)) {
            #COPY TO DB DONE, SAVE STORE ARRAY IN COMPLETED
            echo "Copy successful, clean up can start!\n";
            $rename_n_move = $this->store_array($this->store, $this->complete_dir.$this->active_file);

            if(!$rename_n_move) {
                echo "Problem with copying completed store file to complete store folder\n";
            }else {
                echo "Clean-up store file complete!\n";
            }

        }else {
            #COPY TO DB NOT DONE, SAVE STORE ARRAY IN ERROR AS RETRY
            echo "Problem File not copied to DB, clean-up cannot start, save as retry (".@pg_last_error().")\n";
            $rename_n_move = $this->store_array($this->store, $this->error_dir."retry_".$this->active_file);
        }

        if(is_array($this->trash) && !empty($this->trash)) {
            #trash files have wrong number of columns or bad formats
            $rename_n_move_trash = $this->store_array($this->trash, $this->error_dir."trash_".$this->active_file);

            if(!$rename_n_move_trash) {
                echo "trash file problem.\n";
            }else {
                echo "trash file written.\n";
            }


        }else {
            echo "no trash - good!\n";
        }

        $this->archive_file();
		$now = date('Y-m-d H:i:s');

        echo "-----------------------------------------------\n";
        echo $now."\n";
        echo "======================END======================\n";

    }

    private function archive_file() {
	
        $archive = $this->move_files($this->processing_dir.$this->active_file, $this->archive_dir.$this->active_file);
        if(!$archive) {
            echo "archiving problem.\n";
        }else {
            echo "trash file written.\n";
        }
    }

    private function store_array($array,$location) {
        $target_file = @fopen($location, 'a');
        $write_to_file = @fwrite($target_file, implode("",$array));

        if($write_to_file && $target_file) {
            echo "Array stored in {$location} \n";
            return true;
        }else {
            #IF FAILED TO WRITE CHECK DIR AND MAKE DIR
            $destination_dir = dirname($location);
            $createdir = $this->dir_creator($destination_dir);
            $target_file = @fopen($location, 'a');
            $write_to_file = @fwrite($target_file, implode("",$array));
            if($write_to_file && $target_file) {
                echo "Array stored in FRESH {$location} \n";
                return true;
            }else {
                echo "Could not store array in {$location} \n";
                return false;
            }
        }
        fclose($target_file);
        echo "file closed";
    }

    private function move_files($source,$destination) {
        $rename_n_move = @rename($source, $destination);

        if(!$rename_n_move) {

            $filename = basename($source);
            $destination_dir = dirname($destination);

            #check if source exists
            $file_exists = @file_exists($source);
            #check if destination dir exists, if not create it
            $createdir = $this->dir_creator($destination_dir);
            $rename_n_move_again = @rename($source, $destination);
            if ($file_exists && $createdir && $rename_n_move_again) {
                echo "Fresh Move complete!\n";
                return true;
            }else {
                return false;
                exit("Cannot move files!\n");

            }
        }else {
            echo "Move complete!\n";
            return true;
        }
    }


}

header("Content-Type: text/plain");
date_default_timezone_set('Africa/Accra');
$instance0 = new xdlr_persist();


?>
