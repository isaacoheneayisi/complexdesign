<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

/**
 * Description of PHPClass
 *
 * @author adeshas
 *
 * TASKS
 *
 * create dlr aggregator
 * create file that prevents persistor from runnning multiple times
 * create insert persistor
 * finish url class
 *
 */
class universalclass {

    protected $file;      //file that will be read (TAB delimited)
    protected $sql;      //sql insert file that will be read (sql queries)
    protected $location;  //where the files will be moved for parsing and storage
    protected $processing_dir;
    protected $complete_dir;
    protected $error_dir;
    protected $archive_dir;

    #private $dbhost = "10.188.118.148";
    #private $dbport = "5432";
    #private $dbname = "dlr";
    #private $dbuser = "postgres";
    #private $dbpass = "";
	
    #private $dbhost = "10.164.114.37";
    #private $dbport = "5432";
    #private $dbname = "subengine";
    #private $dbuser = "mtech";
    #private $dbpass = "mtech";
    
    #private $dbhost = "localhost";
    #private $dbport = "5432";
    #private $dbname = "subengine";
    #private $dbuser = "mtech_sub_blaster";
    #private $dbpass = "";

    private $dbhost = "localhost";
    private $dbport = "5432";
    private $dbname = "subengine-ghana";
    private $dbuser = "postgres";
    private $dbpass = "";

    #const file = "csv/dlr_copy.csv";
    #const sql = "csv/dlr_sql_inserts.csv";
    const file = "/usr/local/apache/httpdocs/subengine-ghana/subengine-scripts/persistor/csv/dlr_copy.csv";
    #const file = "/usr/local/apache/httpdocs/subengine-ghana/subengine-scripts/persistor/csv/20130515_150408_dlr.new.csv";
    const sql = "/usr/local/apache/httpdocs/subengine-ghana/subengine-scripts/persistor/csv/dlr_sql_inserts.csv";
    const location = "/usr/local/apache/httpdocs/subengine-ghana/subengine-scripts/persistor/csv/";
    const processing_dir = "/usr/local/apache/httpdocs/subengine-ghana/subengine-scripts/persistor/csv/processing/";
    const complete_dir = "/usr/local/apache/httpdocs/subengine-ghana/subengine-scripts/persistor/csv/completed/";
    const error_dir = "/usr/local/apache/httpdocs/subengine-ghana/subengine-scripts/persistor/csv/error/";
    const archive_dir = "/usr/local/apache/httpdocs/subengine-ghana/subengine-scripts/persistor/csv/archive/";

    public $service_id;
    public $blaster_id;
    public $blast_seq;

    const service_id = '0';
    const blaster_id = '0';
    const blast_seq = '0';

    public function connect_to_db_old() {
        $connect = @pg_connect("host={$this->dbhost} port={$this->dbport} dbname={$this->dbname} user={$this->dbuser} password={$this->dbpass}");
        if (!$connect) {
            echo "Database cannot connect (".@pg_last_error().").\n";
        }else{
			echo "Connected to DB\n";
            return $connect;
        }

    }

    public function connect_to_db() {
	while(!( $connect = @pg_connect("host={$this->dbhost} port={$this->dbport} dbname={$this->dbname} user={$this->dbuser} password={$this->dbpass}"))){
		echo "\n".date('Y-m-d H:i:s')." - "."Database cannot connect (".@pg_last_error().").\n";
		sleep(30);
		echo "\n".date('Y-m-d H:i:s')." - try again!\n";
	}
	echo "Connected to DB \n";
	return $connect;
    }    
    public function set_options($attr,$value) {
        if(property_exists($this,$attr)) {

            if(empty($value)) {
                #assign default value to property
                $this->$attr = constant(get_class($this)."::$attr");
                #$this->$attr = eval("self::$attr;");
            }else {
                #assign given value to property
                #cater for boolean first
                if(strtolower($value) == strtolower("false")) {
                    $value = FALSE;
                }
                if(strtolower($value) == strtolower("true")) {
                    $value = TRUE;
                }
                $this->$attr = $value;
            }

        }else {
            throw new Exception ("Attribute $attr does not exist. \n");
        }
    }

    public function set_all_options() {

        $class_vars = get_object_vars($this);

        foreach ($class_vars as $name => $value) {
            $name = strtolower($name);
            if(isset($_GET[$name]) && !empty($_GET[$name])) {
                $this->set_options($name, $_GET[$name]);
            }else {
                if(defined("static::$name")) {
                    $this->set_options($name, NULL);
                }
            }
        }
    }

    public function show_all_options() {
        $class_vars = get_object_vars($this);

        foreach ($class_vars as $name => $value) {

            echo "$name = $value<br>";


        }
    }

    protected function dir_creator($path) {
        if (!is_dir($path)) {
            echo "Creating folder $path \n";
            $makedir = @mkdir($path,0777,true);
            if($makedir) {
                return true;
            }else {
                return false;
            }
        }else {
            echo "Folder already created $path \n";
            return true;
        }
    }

    protected function get_mno($number) {

	$mno_array = array(
        0 => array('name' => "NONE", 'regex' => "^.+"),
        1 => array('name' => "NIG_GLO", 'regex' => "^(\+?)(0|234)?(705|805|807|815|811|905)"),
        2 => array('name' => "NIG_AIRTEL", 'regex' => "^(\+?)(0|234)?(808|708|802|812|701|902)"),
        3 => array('name' => "NIG_MTN", 'regex' => "^(\+?)(0|234)?(703|706|803|806|810|813|814|816|903)"),
        4 => array('name' => "NIG_ETISALAT", 'regex' => "^(\+?)(0|234)?(809|817|818|909)"),
        5 => array('name' => "NIG_MULTILINKS", 'regex' => "^(\+?)(0|234)?(77|017|177|7027|709|1017|1027|109|1031|1039)"),
        6 => array('name' => "NIG_STARCOMMS", 'regex' => "^(\+?)(0|234)?(180|181|185|187|189|64|74|84|7028|7029|819)"),
        7 => array('name' => "NIG_VISAFONE", 'regex' => "^(\+?)(0|234)?(7025|7026|704)"),
        7 => array('name' => "NIG_MTEL", 'regex' => "^(\+?)(0|234)?(804)"),
        7 => array('name' => "NIG_ZOOM_RELTEL", 'regex' => "^(\+?)(0|234)?(707)"),
        8 => array('name' => "GHA_VODAFONE", 'regex' => "^(\+?)(0|233)?(20|50)"),
        9 => array('name' => "GHA_TIGO", 'regex' => "^(\+?)(0|233)?(27|57)"),
        10 => array('name' => "GHA_EXPRESSO", 'regex' => "^(\+?)(0|233)?(28)"),
        11 => array('name' => "GHA_MTN", 'regex' => "^(\+?)(0|233)?(24|54|55)"),
        12 => array('name' => "GHA_AIRTEL", 'regex' => "^(\+?)(0|233)?(26)"),
        13 => array('name' => "GHA_GLO", 'regex' => "^(\+?)(023|233)?(23)")
        );

        foreach($mno_array as $key => $value) {
            if (preg_match("/".$value['regex']."/", $number)) {
                $mno = array('id'=>$key,'name'=>$value['name']);
            }
        }

		if(empty($mno)){
            $mno = array('id'=>0,'name'=>"NONE");
        }
        return $mno;
    }

}

#$new = new universalclass();
#$new->connect_to_db();
?>
