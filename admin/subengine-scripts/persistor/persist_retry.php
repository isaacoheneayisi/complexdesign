<?php

ini_set('memory_limit', '-1');
/*
 * This file will read all the data from a file in batches and writes to DB
 */
require_once 'universal.php';

class xdlr_persist extends universalclass {

    public $from;
    public $to;
    public $msg;
    public $smsc_id;
    public $smsc;
    public $type;
    public $live;  //persist straight to DB or log to file
    public $writetype; //write to file using linux or php
    private $store;     //where arrays will be stored for "copy from" to DB
    private $trash;     //where bad arrays will be stored for copy later
    private $active_file;   //file being processed
    private $status;    //if true then copy is successful and all files can be cleaned_up
    private $files;

    const type = '0';
    const live = FALSE;
    const writetype = 0; //0 = php (default), 1 = linux

    public function xdlr_persist() {

        $this->set_all_options();
        $this->start_processing();
        
        $path = $this->error_dir;
        $count = 0;
        if ($handle = opendir($path)) {
            while (false !== ($fileread = readdir($handle))) {
                if ('.' === $fileread)
                    continue;
                if ('..' === $fileread)
                    continue;
                #echo "{$path}{$fileread} \n";
                
                $this->active_file = $path.$fileread;
                
                // do something with the file
                echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
                $this->validate();
                $this->persist_to_db();
                $count++;
                echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
            }
            echo "Count contains $count files. \n";
            $this->finish_processing();
            closedir($handle);
        }
    }

    private function persist_to_db() {
        #var_dump($this->store);
        $connect = $this->connect_to_db();
        if ($connect && is_array($this->store)) {

            #CREATE A TEMP TABLE THAT WILL RECEIVE THE COPY RESULTS
            $create = 'CREATE TEMP TABLE _temp_dlr
                        (
                            "date" timestamp,
                            "from" character varying,
                            "to" character varying,
                            msg character varying,
                            service_id integer,
                            mno integer,
                            "type" integer,
                            content_id integer,
                            msg_id character varying,
                            http_status character varying,
                            blaster_id integer,
                            blast_seq integer,
                            dlr_smsc character varying,
                            dlr_submit timestamp,
                            dlr_done timestamp,
                            dlr_status character varying,
                            dlr_duration integer,
                            sub_source character varying
                        )
                        WITH (
                          OIDS=FALSE
                        );';
            pg_query($create);

            #RUN COPY COMMAND
            $copy = pg_copy_from($connect, '_temp_dlr', $this->store, "\t", null);
            if ($copy) {
                echo 'PG COPY Successfull (' . count($this->store) . " items copied)\n";
            } else {
                echo 'PG COPY Error Occured: ' . pg_last_error() . "\n";
            }
         } else {
            echo "Problem with connection or array store. \n";
        }

        if ($copy) {
            #TRANSFER COPIED TEMP TABLE TO MAIN DLR TABLE. TABLE WILL BE DESTROYED AFTER INSTANCE IS DESTROYED
            $query = pg_query('insert into dlr("date","from","to",msg,service_id,mno,"type",content_id, msg_id, http_status,blaster_id, blast_seq, dlr_smsc, dlr_submit, dlr_done, dlr_status, dlr_duration,sub_source) (select "date","from","to",msg,service_id,mno,"type",content_id, msg_id, http_status,blaster_id, blast_seq, dlr_smsc, dlr_submit, dlr_done, dlr_status, dlr_duration,sub_source from _temp_dlr);');
        }
        #IF COPY PROCESS COMPLETES FULLY
        if ($query && $copy) {
            $this->status = 1;
            var_dump($query);
            var_dump($copy);
            echo "DELETE FILE (".$this->active_file.") \n";
            unlink($this->active_file);
        } else {
            $this->status = 0;
        }
        
        pg_close();
        
    }

    private function validate() {
        $this->store = NULL;
        $line_number = 0;
        $handle = @fopen($this->active_file, "r") or die("Couldn't get handle " . $this->active_file . "\n");
        if ($handle) {
            echo "reading file (" . $this->active_file . ")\n";
            while (!feof($handle)) {
                set_time_limit(60);
                #$buffer = fgetcsv($handle, 4096,"\t","dddddd");
                $buffer = fgets($handle, 4096);
                $line_number++;
                #IF LINE IS NOT EMPTY THEN CONTINUE
                if (!empty($buffer)) {

                    #SPLIT LINE INTO COLUMNS AND PUT IN ARRAY
                    $array_of_columns = @split("\t", $buffer);
                    #print_r($array_of_columns);

                    if (count($array_of_columns) == 17) {
                        #FILE HAS RIGHT AMOUNT OF COLUMNS, STORE IN GOOD LIST
                        #echo "GOOD COLS\n";
                        $buffer = str_replace("\n", '', $buffer);
                        $buffer = $buffer . "\t";
                        $this->store[] = $buffer; #implode(",", $array_of_columns);
                    } else if (count($array_of_columns) == 18) {
                        #FILE HAS RIGHT AMOUNT OF COLUMNS, STORE IN GOOD LIST
                        #echo "GOOD COLS\n";
                        $this->store[] = $buffer; #implode(",", $array_of_columns);
                    } else {
                        #FILE HAS WRONG AMOUNT OF COLUMNS, STORE IN BAD LIST
                        #echo "BAD COLS\n";
                        echo "BAD ROW! Line Number:({$line_number}) , ";
                        echo count($array_of_columns)." Columns";
                        echo "\n";
                        $this->trash[] = $buffer;
                    }
                } else {

                    echo "LINE EMPTY!\n";
                }
            }
            echo "Total numbers in file = $line_number \n";
            fclose($handle);
        }
    }

    private function start_processing() {
        echo "=====================START=====================\n";
        $now = date('Y-m-d H:i:s');
        echo $now . "\n";
        echo "-----------------------------------------------\n";



        /*
        if (file_exists($filename)) {
            $rename_n_move = $this->move_files($this->file, $destination . $new_filename);
            #var_dump($rename_n_move);
        } else {
            echo "no file to persist\n";
            $now = date('Y-m-d H:i:s');

            echo "-----------------------------------------------\n";
            echo $now . "\n";
            echo "======================END======================\n";
            exit;
        }
        */
    }

    private function finish_processing() {
        
        $now = date('Y-m-d H:i:s');

        echo "-----------------------------------------------\n";
        echo $now . "\n";
        echo "======================END======================\n";
    }


   

}

header("Content-Type: text/plain");
date_default_timezone_set('Africa/Accra');
$instance0 = new xdlr_persist();
?>