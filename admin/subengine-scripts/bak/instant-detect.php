<?php

# Acts as a detector to see if content needs to be broadcasted. Testing
# Check contents table and compares with blastsummary to see if there is any new content uploaded

debug("START DETECTOR PROCESS",TRUE);

$sql_detect = "
--START DETECTOR QUERY--
    
--GET CONTENT THAT HAS BEEN PROVISIONED FOR THE DAY
select id into temp _s_ from services where status = ".BLASTER_TYPE.";   

select content_id, service_id into temp _cid_sid_toblast_
 from (
select c.id as content_id, c.service_id from content c where valid_date = now()::date and  service_id in (select * from _s_) 

EXCEPT
--CHECK ALL CONTENT THAT HAD BEEN BROADCASTED WITHIN THE LAST COUPLE OF DAYS
select content_id, service_id from blast_summary 
where date::date between now()::date and now()::date
and  service_id in (select * from _s_)
) a;

select * from _cid_sid_toblast_ order by content_id, service_id;

--END DETECTOR QUERY--
";


#RUN QUERY
$detect_query_result = sql_change($sql_detect,$connection);
echo $detect_query_result;


#IF QUERY RETURNS AT LEAST ONE ROW, CONTINUE SCRIPT AND RUN BLASTER ELSE DIE
$row_count = pg_num_rows($detect_query_result);

if ( (int) $row_count >= 1 ){
    $detect_output = "INFO: FOUND {$row_count} CONTENTS UPLOADED. CONTINUE BROADCAST.\n";
    debug($detect_output);
    debug("ENDED DETECTOR PROCESS.\n\n START MAIN BROADCAST\n\n");
}else{
    $detect_output = "INFO: FOUND NO ROWS. WILL TRY AGAIN LATER.\n";
    debug($detect_output,TRUE);
    debug("ENDED DETECTOR PROCESS",TRUE);
    exit;
}   




?>
