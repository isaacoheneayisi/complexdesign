<?php
require_once("blaster-config.php");
require_once("blaster-functions.php");

dir_creator(TEMPDIR);

$user = "nobody";
chown(TEMPDIR, $user);
chgrp(TEMPDIR, $user);
chmod(TEMPDIR,0777);



        
#CONNECT TO DATABASE
$connection_win = connect_to_db(CONN_WINDOWS);
$connection_lin = connect_to_db(CONN_LINUX);

$now = date("Y-m-d",strtotime("yesterday"));
$pre_sql = "

DROP TABLE IF EXISTS __dlr__;
DROP TABLE IF EXISTS __dlr_data3__;
DROP TABLE IF EXISTS __dlr_data4__;

select a.date,a.from,a.to,msg,service_id,type,content_id,blaster_id,blast_seq into temp __dlr__ from dlr a where type = 8 and date::date >= '{$now}';

select b.to, service_id into temp __dlr_data3__ from __dlr__ b where type = 8 and service_id in (
    SELECT id FROM dblink('hostaddr=10.244.62.193 port=5432 dbname=subengine user=mtech password=mtech', 'select id from services where status = 3'
) AS t1(id integer)) and content_id = 0 group by b.to, service_id;

select c.to, service_id into temp __dlr_data4__ from __dlr__ c where type = 8 and service_id in (
    SELECT id FROM dblink('hostaddr=10.244.62.193 port=5432 dbname=subengine user=mtech password=mtech', 'select id from services where status = 4') AS t1(id integer)
) and content_id = 0 group by c.to, service_id;


COPY __dlr_data3__ TO '".TEMPDIR."copy3.txt';
COPY __dlr_data4__ TO '".TEMPDIR."copy4.txt';

";

$sql = "
DROP TABLE IF EXISTS __dlr_data3__;
DROP TABLE IF EXISTS __dlr_data4__;

CREATE TABLE __dlr_data3__
(
  msisdn character varying,
  service_id integer NOT NULL
)
WITH (
  OIDS=FALSE
);

CREATE TABLE __dlr_data4__
(
  msisdn character varying,
  service_id integer NOT NULL
)
WITH (
  OIDS=FALSE
);

\COPY __dlr_data3__ FROM '".TEMPDIR."copy3.txt';
\COPY __dlr_data4__ FROM '".TEMPDIR."copy4.txt';


--ACTIVATE
update subscription_details s
set status = 3, last_bill_time = now()::date
from __dlr_data3__ d
where s.msisdn = d.msisdn and s.service_id = d.service_id;

update subscription_details s
set status = 4, last_bill_time = now()::date
from __dlr_data4__ d
where s.msisdn = d.msisdn and s.service_id = d.service_id;


--DEACTIVATE
update subscription_details d
set status = 0
from services s
where
d.service_id = s.id and
d.status in (3,4) and s.status in (3,4) and
now()::date - last_bill_time::date > billing_period;

";

file_put_contents(TEMPDIR."act-deact.sql", $sql);

$result = pg_query($connection_lin, $pre_sql);
$output = pg_fetch_all($result);
echo pg_last_error($connection_lin);
var_dump($pre_sql);
var_dump($output);

exec("/usr/bin/psql -h localhost -U postgres -f ".TEMPDIR."act-deact.sql"." subengine",$out,$return);
var_dump($out,$return);
?>
