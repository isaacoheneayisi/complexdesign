
<?php
include_once('includes/config.php');
include_once('includes/func.php');
$page_title = "Home Project";
$section = "SMS VOTING "; 
//$success_sms = get_total_success();
//$all_sms = get_total_sms();
//$sms_fail =get_total_fail();
//$success_rate =0;
//if($all_sms > 0)$success_rate = round(($success_sms/$all_sms)* 100); 

include ("header.php"); 



?>
<script type="text/javascript">
    setTimeout(function () { 
      location.reload();
    }, 60 * 1000);
</script>

<style type="text/css">
    
    .img-circle {
    border-radius: 50%;
}

</style>
        <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            
                    <?php include_once('includes/breadcrumbs/breadcrumbs.php'); ?>
                    
            <div class="clearfix"></div>
            <!-- END DASHBOARD STATS 1-->
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <!-- <input type="hidden" id="chartdata" value="<?php echo get_data_for_graph(); ?>" -->
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-globe"></i>All <?php echo ucwords($section) ?> </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                <div class="visual">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="0"><?php echo expected_votes_online(); ?></span>
                                    </div>
                                    <div class="desc"> Total Expected Votes </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                <div class="visual">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="0"><?php echo successful_votes_online(); ?></span> </div>
                                    <div class="desc"> Successful votes  </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                <div class="visual">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                       <!-- <span data-counter="counterup" data-value="0"><?php  //echo $sms_fail; ?></span>-->
                                       <span data-counter="counterup" data-value="0"><?php  echo cancelled_votes_online(); ?></span>
                                    </div>
                                    <div class="desc"> Cancelled votes  </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                                <div class="visual">
                                    <i class="fa fa-bar-chart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">  
                                        <span data-counter="counterup" data-value="0"></span><?php echo number_format((float)(successful_votes_online()/expected_votes_online() *100),2,'.',''); ?>% </div>
                                    <div class="desc"> Succes Rate </div>
                                </div>
                            </a>
                        </div>
                    </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                    
                    

                    <div id="chartdiv"></div>
                    



            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php   
    include ("footer.php");
?>