		var changepassword_form = $("#changepassword-form").validate({
			rules: {
				xpass: "required",
				oldxpass: "required",
				xname: {
					required: true,
					minlength: 3,
					remote: {
					        url: "includes/manage_user.php",
					        type: "post",
					        data: {
					        xxname: function() {
					            return $("#xname").val();
					        },
					        opera: function(){
					        	return 'checkuser';
					        }
					    }
					}
          
				},
				oldxpass: {
					required: true,
					minlength: 1,
					remote: {
					        url: "includes/manage_user.php",
					        type: "post",
					        data: {					        
					        oldxxpassword: function(){
					        	return $( "#oldxpass").val();
					        },
					        opera: function(){
					        	return 'checkpassword';
					        }
					    }
					}
          
				},
				newxpass: {
					required: true,
					minlength: 5
				},
				renewxpass: {
					required: true,
					minlength: 5,
					equalTo: "#newxpass"
				},		
			},
			messages: {
				oldxpass:{
					required: "Please enter a username"
				},
				xname: {
					required: "Please enter a username",
					minlength: "Your username must consist of at least 3 characters"
				},
				newxpass: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				renewxpass: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long",
					equalTo: "Please enter the same password as above"
				},
				
				
			}
		});




$('#changepassword-form').submit(function(e){
e.preventDefault();
console.log($("#changepassword-form").valid());
if($("#changepassword-form").valid()){

	$.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 


    var formData = new FormData($('#changepassword-form')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_user.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
    	$.unblockUI();
        console.log(xhr);
        if(xhr=='1') {
        	window.location.href = 'index.php?islogout=true';
          toastr.success("Password has been saved Successfully", "Status");
           $('#changepassword-form')[0].reset();
        }else {
            toastr.error("Sorry An Error Occured Please Check and Try Again", "Status");
           
        }

    });

  }
 });