var add_user_form = $("#add_pro_stud").validate({
	rules: {
		students_name: "required"		
			
	},
	messages: {
		projects_name: "Please enter name of the project"	
		
	}
});

$('#add_pro_stud').submit(function(e){
e.preventDefault();
console.log($("#add_pro_stud").valid());
if($("#add_pro_stud").valid()){

	$.blockUI({ css: { 
            border: 'none', 
            padding: '15px',
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 


    var formData = new FormData($('#add_pro_stud')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
    	$.unblockUI();
        console.log(xhr);
        if(xhr==true) {
          toastr.success("New Student Has Been Added Successfully", "Status");
           $('#add_students')[0].reset();
        }else {
            toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
           
        }

    });

  }
 });