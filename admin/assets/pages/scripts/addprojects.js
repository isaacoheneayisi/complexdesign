var add_user_form = $("#add_pro").validate({
	rules: {
		projects_name: "required"		
			
	},
	messages: {
		projects_name: "Please enter name of the project"	
		
	}
});

$('#add_pro').submit(function(e){
e.preventDefault();
console.log($("#add_pro").valid());
if($("#add_pro").valid()){

	$.blockUI({ css: { 
            border: 'none', 
            padding: '15px',
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } }); 


    var formData = new FormData($('#add_pro')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
    	$.unblockUI();
        console.log(xhr);
        if(xhr==1) {
          toastr.success("New Student Has Been Added Successfully", "Status");
           $('#add_students')[0].reset();
        }else {
            toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
           
        }

    });

  }
 });