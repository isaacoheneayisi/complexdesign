
$(document).ready(function() {
  setReport();
} );

$.fn.dataTable.ext.errMode = 'none'; $('#view_projectss').on('error.dt', function(e, settings, techNote, message) {
 console.log( 'An error occurred: ', message); 
});

var table = null;

function setReport(){


table = $('#view_projectss').DataTable({
    /*drawCallback: function () {
      var api = this.api();
      $( api.table().column(1).footer() ).html('<span style="color:red;font-weight:700;">' +
        api.column( 1, {page:'current'} ).data().sum() + '</span>'
      );
      $( api.table().column(2).footer() ).html('<span style="color:red;font-weight:700;">' +
        api.column( 2, {page:'current'} ).data().sum() + '</span>'
      );
      $( api.table().column(3).footer() ).html('<span style="color:red;font-weight:700;">' +
        api.column( 3, {page:'current'} ).data().sum() + '</span>'
      );
      $( api.table().column(4).footer() ).html('<span style="color:red;font-weight:700;">' +
        Math.round(api.column( 4, {page:'current'} ).data().sum()/api.column( 4, {page:'current'} ).data().count() * 100)/100 + '%</span>'
      );
    },*/
    "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent": ''
        } ],
    dom: 'Bfrtip',
        buttons: [
       
           'copy','csv','excel','pdf','print'
       
       
       ],
  destroy: true,
     processing: true,
        serverSide: true,        
        searching: true,
        "ajax": {
            url: 'includes/project_report.php',
            type: 'POST'
        },

        "columns": [      

            
            { "data": "student_id"},
            { "data": "action"},
            { "data": "phone_numbers" },
            { "data": "date_recorded"},
            { "data": "text_recorded"}
                              
        ]

} );
}

/*$('#view_partners tbody').on( 'click', 'button', function () {
        var data = table.row( $(this).parents('tr') ).data();
        console.log(data);
        alert( data['accounttype'] +"  "+ data[ 2 ] );
    } );*/

var $modal = $('#ajax-modal');
 
$('#view_projectss tbody').on( 'click', '.change_partner', function(){
var data = table.row( $(this).parents('tr') ).data();
var vid = data['id'];
  // create the backdrop and wait for next modal to be triggered
  $('body').modalmanager('loading');
 
  setTimeout(function(){
     $modal.load('updatepartner.php?id='+vid, '', function(){
      $modal.modal();
    });
  }, 1000);
});


$('#view_projectss tbody').on( 'click', '.add_credit', function(){
var data = table.row( $(this).parents('tr') ).data();
var vid = data['id'];
  // create the backdrop and wait for next modal to be triggered
  $('body').modalmanager('loading');
 
  setTimeout(function(){
     $modal.load('updatecredit.php?id='+vid, '', function(){
      $modal.modal();
    });
  }, 1000);
});


$modal.on('click', 'mm', function(){
  $modal.modal('loading');
  setTimeout(function(){
    $modal
      .modal('loading')
      .find('.modal-body')
        .prepend('<div class="alert alert-info fade in">' +
          'Updated!<button type="button" class="close" data-dismiss="alert">&times;</button>' +
        '</div>');
  }, 1000);
});



/*
    when buttom is pressed
*/
//$(document).ready(function() {
  /*$.ajax({ url: 'includes/func.php',
         data: {action: 'getreport'},
         type: 'post',
         success: function(output) {
          console.log(output);
                      
                  }
}); */   
//setReport();
//} );

/*$('#reportform').submit(function(e){
  e.preventDefault();
  setReport();
});

$.fn.dataTable.ext.errMode = 'none'; $('#report_table').on('error.dt', function(e, settings, techNote, message) {
 console.log( 'An error occurred: ', message); 
});


function setReport(){
  var sdate = $('#sdate').val();
  var edate = $('#edate').val();
  var accid = $('#company').val();


var table = $('#report_table').DataTable({
  drawCallback: function () {
      var api = this.api();
      $( api.table().column(1).footer() ).html('<span style="color:red;font-weight:700;">' +
        api.column( 1, {page:'current'} ).data().sum() + '</span>'
      );
      $( api.table().column(2).footer() ).html('<span style="color:red;font-weight:700;">' +
        api.column( 2, {page:'current'} ).data().sum() + '</span>'
      );
      $( api.table().column(3).footer() ).html('<span style="color:red;font-weight:700;">' +
        api.column( 3, {page:'current'} ).data().sum() + '</span>'
      );
      $( api.table().column(4).footer() ).html('<span style="color:red;font-weight:700;">' +
        Math.round(api.column( 4, {page:'current'} ).data().sum()/api.column( 4, {page:'current'} ).data().count() * 100)/100 + '%</span>'
      );
      $( api.table().column(5).footer() ).html('<span style="color:red;font-weight:700;">GHS ' +
         Math.round(api.column( 5, {page:'current'} ).data().sum()/api.column( 5, {page:'current'} ).data().count() * 10000)/10000 + '</span>'
      );
    },
    dom: 'Bfrtip',
        buttons: [
       {
           extend: 'print',
           footer: true
       },
       {
           extend: 'pdf',
           footer: true
       },
       {
           extend: 'excel',
           footer: true
       },
       {
           extend: 'csv',
           footer: true
       }
       ],
  destroy: true,
     processing: true,
        serverSide: true,        
      searching: false,
        "ajax": {
            url: 'includes/getadminreport.php?',
            data: {"sdate":sdate,"edate":edate,"accountid":accid},
            type: 'POST'
        },

        "columns": [

            { "data": "smswhen2" },

            { "data": "allsms" },

            { "data": "successsms" },

            { "data": "failsms" },
            { "data": "ratex"},
            { "data": "cost"}

        ]

} );
}*/