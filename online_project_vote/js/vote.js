var add_user_form = $("#votes").validate({

	rules: {
		nameCann: "required",
		studentid: "required",
		nationalid: "required",
		school: "required",
		xxname: {
			required: true,
			minlength: 3,
			remote: {
			        url: "includes/manage_user.php",
			        type: "post",
			        data: {
			        xxname: function() {
			            return $( "#xxname").val();
			        },
			        opera: function(){
			        	return 'checkuser';
			        }
			    }
			}
  
		},
		xxpass: {
			required: true,
			minlength: 5
		},
		confirm_xxpass: {
			required: true,
			minlength: 5,
			equalTo: "#xxpass"
		},
		email: {
			required: true,
			email: true
		},
		phone1:"required",
		phone2 :"required",
		fname:"required"			
	},
	messages: {
		studID: "Please enter name student ID",
		nationalID: "Please enter cost per sms",
		phone1:"Please enter telephone number for contact person",
		fname:"Please enter the name of the contact person's first name",
		xxname: {
			required: "Please enter a username",
			minlength: "Your username must consist of at least 3 characters"
		},
		xxpass: {
			required: "Please provide a password",
			minlength: "Your password must be at least 5 characters long"
		},
		confirm_xxpass: {
			required: "Please provide a password",
			minlength: "Your password must be at least 5 characters long",
			equalTo: "Please enter the same password as above"
		},
		email: "Please enter a valid email address"
		
	}
});

$('#votes').submit(function(e){
e.preventDefault();
console.log($("#votes").valid());
if($("#votes").valid()){
  
 $.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } ,
      
        baseZ: 5000

    }); 
 
        setTimeout($.unblockUI, 2000); 


    var formData = new FormData($('#votes')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'includes/manage_post.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
    	$.unblockUI();
        console.log(xhr);
        toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-full-width",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
        if(xhr=='true') {
          toastr.success("Thank you for voting for your favourite contestant", "Status");
           //$('#votes')[0].reset();
           $('#modeclose').click();
        }else {
            toastr.error("Sorry An Error Occured Please Check and Try Again", "Status")
           
        }

    });

  }
 });