<?php
/*
 * For more details
 * please check official documentation of DataTables  https://datatables.net/manual/server-side
 * Coded by charaf JRA
 * RefreshMyMind.com
 */
error_reporting(0);
include_once('config.php');
$conn = pg_connect(LOCAL_CONN );
session_start();
$accountid = $_SESSION['accountid'];
/* IF Query comes from DataTables do the following */
if (!empty($_POST) ) {
$sdate = $_POST['sdate'];
$edate = $_POST['edate'];
$sd = (date_parse_from_format("d/m/Y",$sdate));
$ed = (date_parse_from_format("d/m/Y",$edate));

$startdate = $sd['year'].'-'.$sd['month'].'-'.$sd['day'];
$enddate = $ed['year'].'-'.$ed['month'].'-'.$ed['day'];

    /*
     * Database Configuration and Connection using mysqli
     */
    define("MyTable", "smslog");

    /* END DB Config and connection */

    /*
     * @param (string) SQL Query
     * @return multidim array containing data array(array('column1'=>value2,'column2'=>value2...))
     *
     */

    /* Useful $_POST Variables coming from the plugin */
    $draw = $_POST["draw"];//counter used by DataTables to ensure that the Ajax returns from server-side processing requests are drawn in sequence by DataTables
   /* $orderByColumnIndex  = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
    $orderBy = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
    $orderType = $_POST['order'][0]['dir']; // ASC or DESC
    $start  = $_POST["start"];//Paging first record indicator.
    $length = $_POST['length'];*/
    //Number of records that the table can display in the current draw
    /* END of POST variables */


function getData(){
    global $startdate;
    global $enddate;
    global $conn;
    global $accountid;
    $sql = "SELECT to_char(date_trunc('day',smswhen),'DD-Mon-YYYY') as smswhen2, count(id) as allsms, count(id) filter(where response_code = 0) as successsms, count(id) filter(where response_code = 0) * max(costpersms) as cost,
count(id) filter(where response_code != 0) as failsms, (round((count(id) filter(where response_code = 0)* 1.0 / count(id)*1.0)*100,0)||'%')as ratex
FROM smslog WHERE date_trunc('day',smswhen) >= date_trunc('day','$startdate'::date) AND date_trunc('day',smswhen) <= date_trunc('day','$enddate'::date) AND smsowner = '$accountid'
GROUP BY smswhen2
ORDER BY smswhen2 ASC";

$result = pg_fetch_all(pg_query($conn,$sql));
    $data = array();
    foreach ($result as $row ) {
        $data[] = $row ;
    }
    return $data;
}

function getData2(){
    $orderByColumnIndex  = $_POST['order'][0]['column'];// index of the sorting column (0 index based - i.e. 0 is the first record)
    $orderBy = $_POST['columns'][$orderByColumnIndex]['data'];//Get name of the sorting column from its index
    $orderType = $_POST['order'][0]['dir']; // ASC or DESC
    $start  = $_POST["start"];//Paging first record indicator.
    $length = $_POST['length'];

    global $startdate;
    global $enddate;
    global $conn;
    $sql = "SELECT to_char(date_trunc('day',smswhen),'DD-Mon-YYYY') as smswhen, count(id) as allsms, count(id) filter(where response_code = 0) as successsms, count(id) filter(where response_code = 0) * max(costpersms) as cost,
count(id) filter(where response_code != 0) as failsms, (round((count(id) filter(where response_code = 0)* 1.0 / count(id)*1.0)*100,0)||'%')as ratex
FROM smslog WHERE date_trunc('day',smswhen) >= date_trunc('day','$startdate'::date) AND date_trunc('day',smswhen) <= date_trunc('day','$enddate'::date) AND smsowner = '$accountid'
GROUP BY smswhen
ORDER BY $orderBy $orderType
LIMIT $length OFFSET $start";

$result = pg_fetch_all(pg_query($conn,$sql));
    $data = array();
    foreach ($result as $row ) {
        $data[] = $row ;
    }
    return $data;
}

    

    $recordsTotal = count(getData());

    /* SEARCH CASE : Filtered data */
    if(!empty($_POST['search']['value'])){

        /* WHERE Clause for searching */
        $data = getData2();
        $recordsFiltered = $recordsTotal;
    /* END SEARCH */
}
    else {
        //$sql = sprintf("SELECT * FROM %s ORDER BY %s %s limit %d , %d ", MyTable ,$orderBy,$orderType ,$start , $length);
        $data = getData();
        $recordsFiltered = $recordsTotal;
    }

    /* Response to client before JSON encoding */
    $response = array(
        "draw" => intval($draw),
        "recordsTotal" => $recordsTotal,
        "recordsFiltered" => $recordsFiltered,
        "data" => $data
    );

    echo json_encode($response);

} else {
    echo "NO POST Query from DataTable";
}
?>